import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class Room{
  static ValueNotifier<int> chosenRoom=ValueNotifier(1);
  int floor;
  int number;
  String author;
  List<String> descriptions;
  Room({this.floor, this.number, this.author, this.descriptions});
}