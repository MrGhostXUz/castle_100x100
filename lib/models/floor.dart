import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'room.dart';

class Floor{
  static ValueNotifier<int> chosenFloor=ValueNotifier(1);
  int number;
  List<Room> rooms;
  Floor({this.number, this.rooms});
}