import 'package:castle_100x100/screens/auth.dart';
import 'package:castle_100x100/screens/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:google_sign_in/google_sign_in.dart';

void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Auth.auth = FirebaseAuth.instance;
    Auth.googleSignIn = GoogleSignIn();
    Auth.firestore = FirebaseFirestore.instance;
    print(Auth.auth.currentUser);
    return NeumorphicApp(
      debugShowCheckedModeBanner: false,
      title: 'Qasr',
      materialTheme: ThemeData(
          fontFamily: 'SF Pro',
          visualDensity: VisualDensity.adaptivePlatformDensity,
          platform: TargetPlatform.iOS
      ),
      home: (Auth.auth.currentUser==null)?Auth():Home(),
      // locale: Locale('uz'),
    );
  }
}
