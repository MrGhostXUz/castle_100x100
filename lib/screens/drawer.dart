import 'package:castle_100x100/base.dart';
import 'package:castle_100x100/models/floor.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

import 'home.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 10 * Base.height,
            ),
            Expanded(
              flex: 1,
              child: Image.asset(
                "assets/logo.png",
                width: 40 * Base.width,
                height: 40 * Base.width,
              ),
            ),
            SizedBox(
              height: 2 * Base.height,
            ),
            Container(
              width: 50 * Base.width,
              color: NeumorphicColors.accent,
              height: 0.5,
            ),
            Expanded(
              flex: 5,
              child: Container(
                width: 50 * Base.width,
                child: ValueListenableBuilder(
                  valueListenable: Floor.chosenFloor,
                  builder: (context, chosenFloor, _) => ListView(
                    children: [
                      for (int i = 1; i <= 100; i++)
                        Column(
                          children: [
                            TextButton(
                              onPressed: () {
                                Floor.chosenFloor.value = i;
                                Navigator.pop(context);
                              },
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero),
                              child: Container(
                                width: 50 * Base.width,
                                height: 50,
                                color: (i == chosenFloor)
                                    ? NeumorphicColors.accent.withOpacity(0.2)
                                    : null,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    NeumorphicText(
                                      'Qavat #$i',
                                      style: NeumorphicStyle(
                                          color: NeumorphicColors.accent),
                                    ),
                                    Icon(Icons.apartment_rounded),
                                  ],
                                ),
                              ),
                            ),
                            (i != 100)
                                ? Container(
                                    width: 50 * Base.width,
                                    color: NeumorphicColors.accent,
                                    height: 0.5,
                                  )
                                : Container(),
                          ],
                        ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
