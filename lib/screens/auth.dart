import 'package:animate_do/animate_do.dart';
import 'package:castle_100x100/base.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'home.dart';

class Auth extends StatefulWidget {
  static FirebaseAuth auth;
  static GoogleSignIn googleSignIn;
  static FirebaseFirestore firestore;

  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount =
        await Auth.googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult =
        await Auth.auth.signInWithCredential(credential);
    final User user = authResult.user;

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = Auth.auth.currentUser;
      assert(user.uid == currentUser.uid);

      // print('signInWithGoogle succeeded: $user');

      return '$user';
    }

    return null;
  }

  Future<void> signOutGoogle() async {
    await Auth.googleSignIn.signOut();

    // print("User Signed Out");
  }

  @override
  Widget build(BuildContext context) {
    Base.width = MediaQuery.of(context).size.width * 0.01;
    Base.height = MediaQuery.of(context).size.height * 0.01;
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10 * Base.height),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/logo.png",
                  height: 20 * Base.height,
                  width: 30 * Base.height,
                ),
                SizedBox(
                  height: 15 * Base.height,
                ),
                FadeInUp(
                  delay: Duration(milliseconds: 3000),
                  duration: Duration(milliseconds: 1000),
                  child: TextButton(
                    onPressed: () {
                      signInWithGoogle().then((result) {
                        if (result != null) {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) {
                                return Home();
                              },
                            ),
                          );
                        }
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(
                          0, 2 * Base.height, 0, 2 * Base.height),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset("assets/google_logo.png", width: 25, height: 25,),
                          Padding(
                            padding: EdgeInsets.only(left: 2 * Base.height),
                            child: Text(
                              'Google bilan kirish',
                              style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.lightBlue.shade500,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
