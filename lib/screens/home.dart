import 'package:cached_network_image/cached_network_image.dart';
import 'package:castle_100x100/base.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'auth.dart';
import 'drawer.dart';

class Home extends StatelessWidget {
  static GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  final ScrollController scrollController = ScrollController()
    ..addListener(() {});

  @override
  Widget build(BuildContext context) {
    print(Auth.auth.currentUser.photoURL);
    Base.width = MediaQuery.of(context).size.width * 0.01;
    Base.height = MediaQuery.of(context).size.height * 0.01;
    return Scaffold(
      key: Home.scaffoldKey,
      appBar: NeumorphicAppBar(
        leading: IconButton(
          onPressed: () {
            Home.scaffoldKey.currentState.openDrawer();
          },
          icon: Icon(Icons.menu),
        ),
        title: Text(
          '100x100',
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        actions: [
          Center(
            child: Container(
              height: 40,
              width: 40,
              child: CircleAvatar(
                backgroundImage: CachedNetworkImageProvider(
                  Auth.auth.currentUser.photoURL,
                ),
              ),
            ),
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: Container(
        child: ListWheelScrollView(
          controller: ScrollController(),
          itemExtent: 150.0,
          diameterRatio: 20,
          offAxisFraction: -1.2,
          children: [
            for (int i = 49; i > -1; i--)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  NeumorphicButton(
                    style: NeumorphicStyle(shape: NeumorphicShape.flat),
                    child: NeumorphicText(
                      "#${2 * i + 2}",
                      style: NeumorphicStyle(color: NeumorphicColors.accent),
                      textStyle: NeumorphicTextStyle(fontSize: 20.0),
                    ),
                  ),
                  NeumorphicButton(
                    child: NeumorphicText(
                      "#${2 * i + 1}",
                      style: NeumorphicStyle(color: NeumorphicColors.accent),
                      textStyle: NeumorphicTextStyle(fontSize: 20.0),
                    ),
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
